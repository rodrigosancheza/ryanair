Feature: Ryaneer search
  In order to find items
  As a generic user
  I want to be able to search flights in Ryanair

  Scenario Outline: Ryaneer search
    Given I want to search in Ryanair
    And I want to quit cookies
    When I search for from '<search_origin>' to '<search_destiny>'
    And I search since '<going_date>' to '<return_date>'
    And I search for the number of '<adults>', '<teenagers>', '<children>' and '<babies>' which you like.
    Then I should see '<search_result>'
  Examples:
  | search_origin  | search_destiny   | search_result                |    going_date  |   return_date   | adults  | teenagers | children  | babies  |
  | Madrid         | Alemania         | Madrid a Berlín-Schönefeld   |  10-05-2018    |   12-05-2018    | 3       | 1         | 1         |   3     |
  | Madrid         | Paris            | Madrid a París Beauvais      |  10-05-2018    |   12-05-2018    | 3       | 1         | 1         |   2     |


  @test2
  Scenario: Ryaneer search 2
    Given I want to search in Ryanair
    And I want to quit cookies
    When I search for from 'Madrid' to 'Alemania'
    And I search since '<going_date>' to '<return_date>'
    And I search for the number of '<adults>', '<teenagers>', '<children>' and '<babies>' which you like.
    Then I should see 'Madrid a Berlín-Schönefeld'

  @test3
  Scenario Outline: Ryaneer multiple search
    Given I want to search in Ryanair
    And I want to quit cookies
    When I search for from '<search_origin>' to '<search_destiny>'
    And I search since '<going_date>' to '<return_date>'
    And I search for the number of '<adults>', '<teenagers>', '<children>' and '<babies>' which you like.
    Then I should see '<search_result>'
    Examples:
      | search_origin  | search_destiny   | search_result                |    going_date  |   return_date   | adults  | teenagers | children  | babies  |
      | Madrid         | Alemania         | Madrid a Berlín-Schönefeld   |  10-05-2018    |   12-05-2018    | 3       | 1         | 1         |   1     |
      | Madrid         | Paris            | Madrid a París Beauvais      |  10-05-2018    |   12-05-2018    | 3       | 1         | 1         |   0     |
