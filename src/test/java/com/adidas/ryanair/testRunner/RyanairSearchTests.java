package com.adidas.ryanair.testRunner;


import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = { "src/test/resources/features" },
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"},
        glue = { "com.adidas.ryanair.stepDefinitions" }
)
public class RyanairSearchTests {
}
