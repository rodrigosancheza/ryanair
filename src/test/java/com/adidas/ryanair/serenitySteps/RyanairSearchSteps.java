package com.adidas.ryanair.serenitySteps;

import com.adidas.ryanair.RyanairResultsPage;
import com.adidas.ryanair.RyanairSearchPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;

public class RyanairSearchSteps {
    private RyanairSearchPage searchPage;
    private RyanairResultsPage resultsPage;

    @Step
    public void openRyanairSearchPage(){
        searchPage.open();

    }
    @Step
    public void quitCookies(){
        searchPage.quitCookies();
    }
    @Step
    public void searchOrIn(String searchRequest){
        resultsPage = searchPage.searchOrIn(searchRequest);
    }
    @Step
    public void searchReIn(String searchRequest){
        resultsPage = searchPage.searchReIn(searchRequest);
    }
    @Step
    public void searchFor(String goDa, String reDa){
        resultsPage = searchPage.searchGoDa(goDa);
        resultsPage = searchPage.searchReDa(reDa);

    }
    @Step
    public void searchForNumberOfPerson(int adult, int child, int teenager, int baby){

        resultsPage = searchPage.numberOfPassengers(adult, child, teenager, baby);
        resultsPage = searchPage.pulseGo();
    }
    @Step
    public void verifyResult(String searchResult) {
        List<String> results = resultsPage.getResultsList();
        Assert.assertTrue(results.contains(searchResult));
    }
}
