package com.adidas.ryanair.stepDefinitions;

import com.adidas.ryanair.serenitySteps.RyanairSearchSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class RyanairSearchStepDefinitions {
    @Steps
    RyanairSearchSteps ryanairSearchSteps;

    @Given("I want to search in Ryanair")
    public void iWantToSearchInRayanair() throws Throwable{
        ryanairSearchSteps.openRyanairSearchPage();
    }
    @Given("I want to quit cookies")
    public void iWantToQuitCookies() throws Throwable {
        ryanairSearchSteps.quitCookies();
    }

    @When("I search for from '(.*)' to '(.*)'")
    public void iSearchOrIn(String origin, String destination) throws Throwable {
        ryanairSearchSteps.searchOrIn(origin);
        ryanairSearchSteps.searchReIn(destination);
    }
//    @When("I search for from 'Madrid' to '(.*)'")
//    public void iSearchReIn(String searchRequest) throws Throwable {
//        ryanairSearchSteps.searchReIn(searchRequest);
//    }
    @When("I search since '(.*)' to '(.*)'")
    public void iSearchFor(String goDa, String ReDa) throws Throwable {
        ryanairSearchSteps.searchFor(goDa, ReDa);
    }
    @When("I search for the number of '(.*)', '(.*)', '(.*)' and '(.*)' which you like.")
    public void iSearchForNumberOfPerson(int adult, int child, int teenager, int baby) throws Throwable {
        ryanairSearchSteps.searchForNumberOfPerson(adult, child, teenager, baby);
    }
    @Then("I should see '(.*)'")
    public void iShouldSeeLinkTo(String searchResult) throws Throwable {
        ryanairSearchSteps.verifyResult(searchResult);
    }
}
