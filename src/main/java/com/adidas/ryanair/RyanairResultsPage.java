package com.adidas.ryanair;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class RyanairResultsPage extends PageObject {

    @FindBy(xpath = "//h1")
    private List<WebElement> levelOneHeader;

    public RyanairResultsPage(WebDriver driver){
        super(driver);
    }

//    @WhenPageOpens
//    public void waitForPage() {
//        //This class waits for Ryanair sub-menu.
//        $("#booking-selection > article > div.flight-title > div.trip-summary-info.pull-left > h1 > translate > span.\\5c \\22 destination\\5c \\22").waitUntilVisible();
//    }

    public List<String> getResultsList() {
        List<String> resultList = new ArrayList<>();
        for (WebElement element : levelOneHeader) {
            resultList.add(element.getText());
        }
        return resultList;
    }

}
