package com.adidas.ryanair;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.cssSelector;

@DefaultUrl("https://www.ryanair.com/es/es/")
public class RyanairSearchPage extends PageObject{
//  Another choice to instantiate @FindBy
//    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/form/div[2]/div/div/div[2]/div[2]/div[2]/div/div[1]/input")
//    private WebElement searchOriginInput;
    @FindBy(css = "input[placeholder='Aeropuerto de salida']")
    private WebElement searchOriginInput;

    @FindBy(css = "input[placeholder='Aeropuerto de destino']")
    private WebElement searchDestinyInput;

    // It`s used to open the navigator.
    public RyanairSearchPage (WebDriver driver){
        super(driver);
    }

//    @WhenPageOpens
//    public void waitUntilIconAppears(){
//        $("#home > div.header > smart-search > div > div.row-header > div > smart-search-type-selector > ul > li.flights-tab.selected > label > div > core-icon").waitUntilVisible();
//    }

    public RyanairResultsPage searchOrIn(String searchRequest){
//        element(searchOriginInput).clear();
//        element(searchOriginInput).typeAndEnter(searchRequest);
        searchOriginInput.clear();
        searchOriginInput.sendKeys(searchRequest);
        searchOriginInput.sendKeys(Keys.ENTER);
        return new RyanairResultsPage(getDriver());

    }
    public RyanairResultsPage quitCookies(){
//        element(searchOriginInput).clear();
//        element(searchOriginInput).typeAndEnter(searchRequest);
        WebElement cookie = getDriver().findElement(cssSelector("core-icon[class='close-icon']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(cookie).click();
        return new RyanairResultsPage(getDriver());
    }
    public RyanairResultsPage searchReIn(String searchRequest) {
        element(searchDestinyInput).clear();
        element(searchDestinyInput).typeAndEnter(searchRequest);
//        searchDestinyInput.clear();
//        searchDestinyInput.sendKeys(searchRequest);
//        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(searchRequest);
//        WebElement returnInput = getDriver().findElement(By.cssSelector("#search-container > div:nth-child(1) > div > form > div.col-flight-search-left > div.row-airports > div > div.col-destination-airport > div.core-route-selector.popup-destination-airport.opened > div > div > div.content > popup-content > core-linked-list > div.pane.right > div.core-list-small > div.core-list-item.core-list-item-rounded.initial > span"));
//        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(returnInput).click();
        return new RyanairResultsPage(getDriver());
    }

    public RyanairResultsPage searchGoDa(String goDa){
//        element(searchGoingDate).clear();
//        element(searchGoingDate).typeAndEnter(searchRequest);
//        searchGoingDate.clear();
//        searchGoingDate.sendKeys(searchRequest);
//        Calendar calendar = Calendar.getInstance();
//        String[] dates = new String[];


//        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(date).click();
        WebElement dayOr = getDriver().findElement(cssSelector("li[data-id='"+goDa+"']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dayOr).click();
//        day.click();
//        element(day).click();
//        clickOn(day);
//        searchGoingDate.sendKeys(Keys.ENTER);
        return new RyanairResultsPage(getDriver());
    }

    public RyanairResultsPage searchReDa(String reDa){
//        element(searchGoingDate).clear();
//        element(searchGoingDate).typeAndEnter(searchRequest);
//        searchGoingDate.clear();
//        searchGoingDate.sendKeys(searchRequest);
        WebElement dayRe = getDriver().findElement(cssSelector("li[data-id='"+reDa+"']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dayRe).click();
//        day.click();
//        element(day).click();
//        clickOn(day);
//        searchGoingDate.sendKeys(Keys.ENTER);
        return new RyanairResultsPage(getDriver());
    }

    public RyanairResultsPage numberOfPassengers(int adult, int child, int teenager, int baby) {
        WebElement dropdown = getDriver().findElement(cssSelector("core-icon[class=\"chevron\"]"));
        withTimeoutOf(10,TimeUnit.SECONDS).waitFor(dropdown).click();
        for (int i=1; i<adult; i++){
            WebElement numberOfAdults = getDriver().findElement(By.xpath("//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[6]/div/div[3]/core-inc-dec/button[2]"));
            withTimeoutOf(10, TimeUnit.SECONDS).waitFor(numberOfAdults).click();
        }

        for (int j=0; j<teenager; j++){
            WebElement numberOfTeenagers = getDriver().findElement(By.xpath("//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[7]/div/div[3]/core-inc-dec/button[2]"));
            withTimeoutOf(10, TimeUnit.SECONDS).waitFor(numberOfTeenagers).click();
        }

        for (int i=0; i<child; i++){
            WebElement numberOfChildren = getDriver().findElement(By.xpath("//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[8]/div/div[3]/core-inc-dec/button[2]"));
            withTimeoutOf(10, TimeUnit.SECONDS).waitFor(numberOfChildren).click();
        }
        if (baby != 0) {
            String a = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[9]/div/div[3]/core-inc-dec/button[2]";
            WebElement numberOfBabies = getDriver().findElement(By.xpath("//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[9]/div/div[3]/core-inc-dec/button[2]"));
            withTimeoutOf(10, TimeUnit.SECONDS).waitFor(numberOfBabies).click();
//            WebElement aceptWarning = getDriver().findElement(By.cssSelector("span[translate=\"foh.home.flight_search_infant_popup.ok\"]"));
//            withTimeoutOf(10, TimeUnit.SECONDS).waitFor(aceptWarning).click();
            WebElement jajaja = getDriver().findElement(cssSelector("div.dialog-body button.core-btn-primary"));
            jajaja.click();
            By loadingImage = new By.ByXPath("//*[@id=\"ngdialog4\"]/div[2]/div[1]/div/div/div[2]");
            withTimeoutOf(10, TimeUnit.SECONDS).waitForElementsToDisappear(loadingImage);
            By putoLoading = new By.ByCssSelector("div.text-center flight-search-dialog__footer");

//            WebDriverWait wait = new WebDriverWait(getDriver(), 10);
//            wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingImage));

            withTimeoutOf(10, TimeUnit.SECONDS).waitForElementsToDisappear(putoLoading);
            for (int i=1; i<baby; i++){
//                withTimeoutOf(10,TimeUnit.SECONDS).elementIsCurrentlyVisible(By.xpath(a));
                withTimeoutOf(10,TimeUnit.SECONDS).waitFor(numberOfBabies).click();
            }

        }
        return new RyanairResultsPage(getDriver());
    }

    public RyanairResultsPage pulseGo(){
//        /html/body
        WebElement pulse = getDriver().findElement(cssSelector("div[class=dropdown-handle]"));
        withTimeoutOf(10,TimeUnit.SECONDS).waitFor(pulse).click();

        WebElement pulse2= getDriver().findElement(cssSelector("span[class=terms-conditions-checkbox-span]"));
        withTimeoutOf(10,TimeUnit.SECONDS).waitFor(pulse2).click();

        WebElement pulse3 = getDriver().findElement(cssSelector("button[class=\"core-btn-primary core-btn-block core-btn-big\"]"));
        withTimeoutOf(10,TimeUnit.SECONDS).waitFor(pulse3).click();
        return new RyanairResultsPage(getDriver());
    }
}
